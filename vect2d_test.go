package vect2d

import (
	"math"
	"testing"
)

func TestDotProduct(t *testing.T) {

	a := [2]float64{3, 1}
	b := [2]float64{0, 2}
	ans := DotProduct(a, b)
	if ans != float64(2.0) {
		t.Errorf("expected 2 got %10.6f", ans)
	}
}
func TestMagnitude(t *testing.T) {

	a := [2]float64{3, 1}
	ans := Magnitude(a)
	if ans != float64(3.162277660168379331) {
		t.Errorf("expected 3.162277660168379331  got %10.6f", ans)
	}
}
func TestGetAngle(t *testing.T) {

	a := [2]float64{1, 0}
	b := [2]float64{1, 1.7320508075688772935274463415059}
	ans := math.Abs(GetAngle(a, b) - 0.5000)
	if ans > float64(0.000001) {
		t.Errorf("expected 0.50000 got %10.6f", ans)
	}

}
func TestSubtract(t *testing.T) {

	a := [2]float64{1, 0}
	b := [2]float64{1, 1}
	c := [2]float64{0, -1}
	ans := Subtract(a, b)
	if ans != c {
		t.Errorf("expected 0,-1 got %x", ans)
	}

}
func TestAdd(t *testing.T) {

	a := [2]float64{1, 0}
	b := [2]float64{1, 1}
	c := [2]float64{2, 1}
	ans := Add(a, b)
	if ans != c {
		t.Errorf("expected 2,1 got %x", ans)
	}

}
func TestScalerMult(t *testing.T) {

	a := [2]float64{1, 2}
	b := float64(5)
	c := [2]float64{5, 10}
	ans := ScalerMult(a, b)
	if ans != c {
		t.Errorf("expected 5,10 got %x", ans)
	}

}
func TestNormalize(t *testing.T) {

	a := [2]float64{2, 0}
	b := [2]float64{1, 0}
	ans := Normalize(a)
	if ans != b {
		t.Errorf("expected 1,0 got %x", ans)
	}

}
